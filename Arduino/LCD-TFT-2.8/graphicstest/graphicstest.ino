// (c) Michael Schoeffler 2019, http://www.mschoeffler.de

#include <SPI.h>
#include <SD.h>
#include <TFT.h> 

#define PIN_SD_CS 4
#define PIN_TFT_CS 10
#define PIN_DC 9
#define PIN_RST 8

#define DELAY_IMAGE_SWAP 60000 // each image is shown for 60 seconds

TFT TFTscreen = TFT(PIN_TFT_CS, PIN_DC, PIN_RST);

void setup() {
  // initialize default serial connection to send debug information
  Serial.begin(9600);
  while (!Serial) {
    // wait until default serial connection is fully set up
  }
  
  //The following two lines replace "TFTscreen.begin();" => avoids that red and blue (?) are swapped/interchanged
  TFTscreen.initR(INITR_BLACKTAB);
  TFTscreen.setRotation(1);
  
  
  TFTscreen.background(255, 255, 255); // prints black screen to TFT display
  