//https://miliohm.com/spp-c-and-hc-05-bluetooth-module-with-arduino-tutorial/

#include <SoftwareSerial.h>
#include <Wire.h> 
//#include <LiquidCrystal_I2C.h>
//LiquidCrystal_I2C lcd(0x27,16,2);
SoftwareSerial mySerial(3, 4); //Rx,Tx
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  mySerial.begin(9600);
//  lcd.init();
 // lcd.backlight();
 // lcd.clear();
}
void loop() {
  String data;
  if (mySerial.available()) {
    data += mySerial.readString();
    data = data.substring(0, data.length() - 2);
    Serial.print(data+"\n");// add new line in receive
  //  lcd.setCursor(0,0);
  //  lcd.print(data);
    
  }
  if (Serial.available()) {
    mySerial.write(Serial.read());
  }
}