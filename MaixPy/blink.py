from fpioa_manager import fm
from Maix import GPIO
import time

io_led_red = 13# tu chodzi o IO13 a nie 13 na płytce. IO13 to 9 na płytce
fm.register(io_led_red, fm.fpioa.GPIO0)

led_r=GPIO(GPIO.GPIO0, GPIO.OUT)
led_r.value(0)

while True:
    led_r.value(1)
    time.sleep(2)
    led_r.value(0)
    time.sleep(2)
